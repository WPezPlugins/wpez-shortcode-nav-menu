<?php


namespace WPezShortcodeNavMenu\App;

use WPezShortcodeNavMenu\App\Core\HooksRegister\ClassHooksRegister;
use WPezShortcodeNavMenu\App\Plugin\Shortcode\ClassShortcode as Shortcode;


class ClassPlugin
{


    protected $_str_sc_tag;

    protected $_new_hooks_reg;
    protected $_arr_actions;
    protected $_arr_filters;


    public function __construct()
    {

        $this->setPropertyDefaults();

        $this->addShortcode();

        /*
        $this->actions(true);

        $this->filters(true);

        $this->other(false);

        $this->wpCore(false);

        // this should be last
        $this->hooksRegister();
        */

    }

    protected function setPropertyDefaults()
    {

        $str_tag = '';
        $str_tag = apply_filters('wpez_shortcode_nav_menu_tag', $str_tag);
        $this->_str_sc_tag = 'wpez_scnm';
        if ( is_string($str_tag) && ! empty($str_tag)){
            $this->_str_sc_tag = $str_tag;
        }

        $this->_new_hooks_reg = new ClassHooksRegister();
        $this->_arr_actions = [];
        $this->_arr_filters = [];


    }

    protected function addShortcode(){

        $new = new Shortcode();

        $new->setTag($this->_str_sc_tag);
        add_shortcode($this->_str_sc_tag, [$new, 'getShortcode']);
    }

    /**
     * After gathering (below) the arr_actions and arr_filter, it's time to
     * make some RegisterHook magic
     */
    protected function hooksRegister()
    {

        $this->_new_hooks_reg->loadActions($this->_arr_actions);

        $this->_new_hooks_reg->loadFilters($this->_arr_filters);

        $this->_new_hooks_reg->doRegister();

    }


    public function actions($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new_actions = new ClassActions();


        $this->_arr_actions[] = [
            'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
            'hook' => 'suki/frontend/header',
            'component' => $new_actions,
            'callback' => 'removeSukiFrontendHeader',
            'priority' => 5
        ];


        // remove the featured image on pages (only)
        $this->_arr_actions[] = [
            'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
            'hook' => 'suki/frontend/entry/before_header',
            'component' => $new_actions,
            'callback' => 'removeSukiFrontendEntryBeforeHeader',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true

            'hook' => 'suki/frontend/entry/header',
            'component' => $new_actions,
            'callback' => 'sukiFrontendEntryHeaderSingle',
            'priority' => 10
        ];

        $this->_arr_actions[] = [
            'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
            'hook' => 'suki/frontend/entry/header',
            'component' => $new_actions,
            'callback' => 'sukiFrontendEntryHeaderBlogIndex',
            'priority' => 30
        ];


    }

    public function filters($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new_filters = new ClassFilters();

        // prevents the page featured image from being a background image in the header title bar
        $this->_arr_filters[] = [
            'hook' => 'wp_get_attachment_image_src',
            'component' => $new_filters,
            'callback' => 'wpGetAttachmentImageSrc',
            'priority' => 75,
            'accepted_args' => 4
        ];
    }


    public function other($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new_other = new ClassOther();
    }


    public function wpCore($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $this->wpDequeueStyle(false);

        $this->wpDequeueScript(false);

        $this->deregisterPostType(false);

        $this->postTypeSupport(false);

        $this->unregisterSidebar(false);

        $this->unregisterWidget(false);

        $this->removeSetting(false);
    }


    protected function wpDequeueStyle($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'style-name-1' => true, // true = remove
            // 'style-name-2' => false // false = leave
        ];
        $this->_new_wpcore->setStyles($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'wp_enqueue_scripts',
            'component' => $this->_new_wpcore,
            'callback' => 'wpDequeueStyle',
            'priority' => 100
        ];
    }


    protected function wpDequeueScript($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'script-name-1' => true,
            // 'script-name-2' => false
        ];
        $this->_new_wpcore->setScripts($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'wp_enqueue_scripts',
            'component' => $this->_new_wpcore,
            'callback' => 'wpDequeueScript',
            'priority' => 100
        ];
    }


    protected function deregisterPostType($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'post-type-1' => true,
            // 'post-type-2' => false
        ];
        $this->_new_wpcore->setPostTypes($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'deregisterPostType',
            'priority' => 100
        ];
    }


    protected function postTypeSupport($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'support-1' => true,
            // 'support-2' => false
        ];
        $this->_new_wpcore->setPostTypesSupport($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'removePostTypeSupport',
            'priority' => 100
        ];

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'addPostTypeSupport',
            'priority' => 105
        ];
    }


    protected function unregisterSidebar($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'sidebar-name-1' => true,
            // 'sidebar-name-2' => false
        ];
        $this->_new_wpcore->setSidebars($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'unregisterSidebar',
            'priority' => 100
        ];

    }


    protected function unregisterWidget($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'widget-name-1' => true,
            // 'widget-name-2' => false
        ];
        $this->_new_wpcore->setWidgets($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'unregisterWidget',
            'priority' => 100
        ];

    }

    protected function removeSetting($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        // Don't forget to use set_theme_mod() to "replace" the values for the settings you remove
        $arr_args = [
            // 'setting-name-1' => true,
            // 'setting-name-2' => false
        ];
        $this->_new_wpcore->setSettings($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'customize_register',
            'component' => $this->_new_wpcore,
            'callback' => 'removeSetting',
            'priority' => 100
        ];

    }
}