<?php

namespace WPezShortcodeNavMenu\App\Plugin\Shortcode;

// No WP? Die! Now!!
if (!defined('ABSPATH')) {
    header('HTTP/1.0 403 Forbidden');
    die();
}

class ClassShortcode
{

    protected $_str_sc_tag;
    protected $_arr_nav_menu_defaults;

    public function __construct()
    {
        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults()
    {

        // ref: https://developer.wordpress.org/reference/functions/wp_nav_menu/

        $arr_nav_menu_defaults = array(
            'active' => true,   // added, these atts does not exist in wp_nav_menu()
            'title_tag' => 'div', // added, these atts does not exist in wp_nav_menu()
            'menu' => '',
            'container' => 'nav', // wp default: div
            'container_class' => 'wpez-scnm', // wp default: ''
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',  // note: wp_nav_menu do NOT check to make sure this a valid ID
            'echo' => true,   // note: this is set to false below.
            'fallback_cb' => [$this, 'fallbackCB'], // wp default: wp_page_menu
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'item_spacing' => 'preserve',
            'depth' => 0,
            'walker' => '',
            'theme_location' => '',
        );

        $this->_arr_nav_menu_defaults = $arr_nav_menu_defaults;
        $arr_nav_menu_defaults_filter = apply_filters('wpez_shortcode_nav_menu_args_defaults', []);

        if (is_array($arr_nav_menu_defaults_filter)) {
            $this->_arr_nav_menu_defaults = array_merge($arr_nav_menu_defaults, $arr_nav_menu_defaults_filter);
        }

    }

    public function fallbackCB($atts, $content = "")
    {
        return '<!-- fallback_cb - menu doesn\'t exist -->';
    }

    public function setTag($str = false)
    {

        if (is_string($str) && !empty($str)) {

            $this->_str_sc_tag = $str;
            return true;
        }
        return false;

    }


	public function getShortcode($atts, $content = ""){

		$arr_atts = shortcode_atts( $this->_arr_nav_menu_defaults, $atts,  $this->_str_sc_tag );

		if ( $arr_atts['active'] !== true){
			return '<!-- active !== true -->';
		}

		$arr_atts['echo'] = false;

		global $_wp_registered_nav_menus;

		if ( ! is_array($_wp_registered_nav_menus)){
			return '<!-- no registered menus -->';
		}

		if ( ! isset($_wp_registered_nav_menus[$arr_atts['theme_location']] ) ){
			return '<!-- requested theme_location: ' . esc_attr($arr_atts['theme_location']) . ' is not a registered menu -->';
		}

		$int = $this->getMenuObject($arr_atts['theme_location'], 'count' );

		if ( ! is_integer($int) || $int == 0 ){
			return '';
		}

		$str_title_markup = '';
		$str_title_flag = 'title-false';
		if ( $arr_atts['title'] == 'menu_name' || $arr_atts['title'] == 'custom'){
			//  if ( ! empty($content)){

			$str_title = false;
			if ( $arr_atts['title'] == 'custom' && ! empty(esc_html($content)) ) {
				$str_title = $content;
			} elseif ( $arr_atts['title'] == 'menu_name' ) {

				$mix = $this->getMenuObject($arr_atts['theme_location'], 'name' );

				if ( is_string($mix ) && ! empty($mix) ){
					$str_title = $mix;
				}

			}

			if ( is_string($str_title) ) {

				$str_title_flag = 'title-true';
				$str_title_markup .= '<' . esc_attr($arr_atts['title_tag']);
				$str_title_markup .= ' class="' . esc_attr($arr_atts['container_class']) . '-title">';
				$str_title_markup .= esc_html($str_title);
				$str_title_markup .= '</' . esc_attr($arr_atts['title_tag']) . '>';
			}
		}

		$str_open = '<div class="'. esc_attr($arr_atts['container_class']). '-wrapper ' . esc_attr($str_title_flag) .'">';
		$str_close = '</div>';

		return $str_open . $str_title_markup . wp_nav_menu($arr_atts) . $str_close;

	}

	protected function getMenuObject( $str_theme_location = false, $str_property = false ){

    	$arr_nav_menu_locations = get_nav_menu_locations();

		if ( ! isset($arr_nav_menu_locations[$str_theme_location])){
			return '';
		}
		$obj_menu = get_term( $arr_nav_menu_locations[$str_theme_location], 'nav_menu' );

		if ( ! $obj_menu instanceof \WP_Term){
			return '';
		}

		if ( ! is_string( $str_property) ){
			return $obj_menu;
		}elseif ( property_exists($obj_menu, $str_property)){
			return $obj_menu->$str_property;
		}

		return '';

	}
}
