## WPezsShortcodeNavMenu

__Use a WordPress shortcode to add a wp_nav_menu() location to your theme/content. Also works with the Gutenberg shortcode block.__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

Shortcode name: wpez_scnm

Aside from all the args for wp_nav_menu (link below) you can also pass in:

- 'active' - Any value will cause the shortcode to return '' (empty string).

- 'title_tag' - Default value: 'div'. What tag do you wish to wrap the title in? 

To add a title, simply put that text within the shortcode tags. For example:

[wpez_scnm theme_location='my-new-location']This is my title[/wpez_scnm]

Note: This plugin will return '' (empty string) if theme_location is not specified, or if the value of theme_location is not a valid registered nav location. This is slightly different than the default behaviour of wp_nav_menu(). 


> --
>
> IMPORTANT
>
> This plugin will not register your menus (i.e., register_nav_menus()). It simply allows you to place a registered menu via a shortcode / Gutenberg block. 
>
> -- 

In short, register your navs, use Admin > Appearance > Menus to manage them, and then this plugin will let you place a given theme_location via shortcode / shortcode block.

### FAQ

__1 - Why?__

I needed a quick and straightforward way to place a nav menu in a page/post when using Gutenberg. 


__2 - This doesn't follow the WP coding standards / naming convention?__

Yup. Don't panic. That's okay. Everything is gonna be alright.


**3 - I'm not a developer, can we hire you?**

Yes, that's always a possibility. If I'm available, and there's a good mutual fit.


### Helpful Links

- https://codex.wordpress.org/Function_Reference/register_nav_menus

- https://developer.wordpress.org/reference/functions/wp_nav_menu/


### TODO

- Add and/or document the filters


### CHANGE LOG

__-- 0.0.6 - Sunday 3 Nov 2019__

- UPDATED - Now uses menu title which can be disabled or overridden.

__-- 0.0.5 - Wed 11 Sept 2019__

- INIT - Hey! Ho!! Let's go!!!

   